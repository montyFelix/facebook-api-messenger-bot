'use strict'

const request_promise = require('request-promise');
const PAGE_ACCESS_TOKEN = require('../config').PAGE_ACCESS_TOKEN;

//helper to get a user information
//name, avatar, etc.
function getUserInformation(psid) {
  // Send the HTTP request to the Messenger Platform
  return request_promise({
    "uri": `https://graph.facebook.com/v2.6/${psid}?fields=first_name,last_name,profile_pic`,
    "qs": { "access_token": PAGE_ACCESS_TOKEN },
    "json": true
  }).then((data) => {
    return data
  })
  .catch((err) => console.log(err))
}

module.exports = getUserInformation;
