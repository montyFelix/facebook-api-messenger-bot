'use strict'

const {findMessage} = require('../services/messageService');
const {callSendAPI} = require('./handleMessaging');

//helper to send a last message right away,
//a message doesn't go to a db
function lastMessageResponse(sender_psid) {
  //using messageService
  return findMessage(sender_psid)
    //don't know what's with that, it's sending data to err catch
    // but if you get data from there, everithing wokrs :)
    .then(() => {console.log('gg')}, (data) => {
      let response;
      if (data[0].message) {
        response = {
          "text": data[0].message
        }
      } else if (data[0].attachments) {
        response = {
          "attachment": data[0].attachments[0]
        }
      }

      //sending last message back to user
      callSendAPI(sender_psid, response)
    });
}

module.exports = lastMessageResponse;
