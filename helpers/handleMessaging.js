'use strict'

const request = require('request');

const {addMessage} = require('../services/messageService');

const PAGE_ACCESS_TOKEN = require('../config').PAGE_ACCESS_TOKEN;

//checking for nlp entities in message
function firstEntity(nlp, name) {
  return nlp && nlp.entities && nlp.entities[name] && nlp.entities[name][0];
}

// Handles messages events
function handleMessage(userData, received_message) {
  //checking for nlp entities in message
  const greeting = firstEntity(received_message.nlp, 'greetings');
  const bye = firstEntity(received_message.nlp, 'bye');
  const thanks = firstEntity(received_message.nlp, 'thanks');
  
  const sender_psid = userData.id;
  let response;

  // check greeting is here and is confident
  if (greeting && greeting.confidence > 0.8) {
    response = {
      "text": `Hello! ${userData.first_name}`
    }
  //check if there's a bye in message
  } else if (bye && bye.confidence > 0.8) {
    response = {
      "text": `Good Bye! ${userData.first_name}`
    }
  //check if there's a thanks in message
  } else if (thanks && thanks.confidence > 0.8) {
    response = {
      "text": `Your Welcome, ${userData.first_name}. I'm Here for You!`
    }
  //check if there's a text in message
  } else if (received_message.text.includes('live') ||
           received_message.text.includes('real') &&
           received_message.text.includes('you')) {
    response = {
      "text": `I myself, exist, because I think.`
    }
  //check if there's a text in message
  } else if (received_message.text.includes('you') ||
           received_message.text.includes('have') &&
           received_message.text.includes('name')) {
    response = {
      "text": `I am QT1, ${userData.first_name}. You can call me Cutie.`
    }
  //check if there's a text in message
  } else if (received_message.text) {
    response = {
      "text": `You wrote: ${received_message.text}.`
    }
  //otherwise there has to be an attachment
  } else if (received_message.attachments) {
    // Gets the URL of the message attachment
    let attachment_url = received_message.attachments[0].payload ?
    received_message.attachments[0].payload.url : received_message.text;
    response = {
      "attachment": {
        "type": "template",
        "payload": {
          "template_type": "generic",
          "elements": [{
            "title": "Is this the right picture?",
            "subtitle": "Tap a button to answer.",
            "image_url": attachment_url,

            "buttons": [
              {
                "type": "postback",
                "title": "Yes!",
                "payload": "yes",
              },
              {
                "type": "postback",
                "title": "No!",
                "payload": "no",
              }
            ],
          }]
        }
      }
    }
  }

  // Sends the response message
  callSendAPI(sender_psid, response);
  //Adds a message to database
  addMessage({received_message, userData}, (err) => {
    if (err) {
      console.log(err.errors);
    }
  });
}

// Handles messaging_postbacks events
function handlePostback(sender_psid, received_postback) {
  let response;

  // Get the payload for the postback
  let payload = received_postback.payload;

  // Set the response based on the postback payload
  if (payload === 'yes') {
    response = { "text": "Thanks!" }
  } else if (payload === 'no') {
    response = { "text": "Oops, try sending another image." }
  }
  // Send the message to acknowledge the postback
  callSendAPI(sender_psid, response);
}

// Sends response messages via the Send API
function callSendAPI(sender_psid, response) {
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "message": response
  }

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": { "access_token": PAGE_ACCESS_TOKEN },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console.log('message sent!')
    } else {
      console.error("Unable to send message:" + err);
    }
  });
}

module.exports.handleMessage = handleMessage;
module.exports.handlePostback = handlePostback;
module.exports.callSendAPI = callSendAPI;
