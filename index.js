'use strict';


const config = require('./config');
const PAGE_ACCESS_TOKEN = config.PAGE_ACCESS_TOKEN;

// Imports dependencies and set up http server
const express = require('express'),
  request = require('request'),
  bodyParser = require('body-parser'),
  path = require('path'),
  mongoose = require('mongoose');

const getUserInformation = require('./helpers/getUserInfo');
const lastMessageResponse = require('./helpers/lastMessage');
const { handleMessage, handlePostback } = require('./helpers/handleMessaging');
// const messageService = require('./services/messageService');

mongoose.connect(config.mongoUri)

// create express http server
const app = express().use(bodyParser.json());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// app.use(express.static(path.join(__dirname, 'public')));

// Sets server port and logs message on success
app.listen(1337, () => console.log(`webhook is listening`));

app.get('/hello', (req, res) => {

	res.render('index', {message: 'req.body.object'});

});
// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {

  let body = req.body;
  // console.log(body.entry[0].messaging);

  // Checks this is an event from a page subscription
  if (body.object === 'page') {

    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function(entry) {

      // Gets the message. entry.messaging is an array, but
      // will only ever contain one message, so we get index 0
      let webhook_event = entry.messaging[0];

      // Get the sender PSID
      let sender_psid = webhook_event.sender.id;

      //check if a message is a call for last message
      if (webhook_event.message && webhook_event.message.text == 'last message') {
        lastMessageResponse(sender_psid);
      // Check if the event is a message or postback and
      // pass the event to the appropriate handler function
      } else if (webhook_event.message) {
        getUserInformation(sender_psid).then((data) => {
          handleMessage(data, webhook_event.message);
        });
      } else if (webhook_event.postback) {
        handlePostback(sender_psid, webhook_event.postback);
      }
    });

    // Returns a '200 OK' response to all requests
    res.status(200).send('EVENT_RECEIVED');
  } else {
    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }

});

// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {

  // Your verify token. Should be a random string.
  let VERIFY_TOKEN = config.VERIFY_TOKEN;

  // Parse the query params
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];

  // Checks if a token and mode is in the query string of the request
  if (mode && token) {

    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {

      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);

    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});
