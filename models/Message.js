'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Message Schema
const MessageSchema = new Schema({
  psid: {type:Number, required: "Please do your job:)"},
  messageSeq: {type: Number, required: "Please do your job:)"},
  full_name: {type: String, required: "Please emter a user name"},
  message: {type: String, default: ""},
  attachments: {type: Array, default: []},
  created: {type: Date, default: Date.now()}
});

let Message = mongoose.model('Message', MessageSchema)

module.exports = {
  Message
};
