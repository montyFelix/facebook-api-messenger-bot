'use strict'

const Message = require('../models/Message').Message;
const MessageModel = require('../models/Message');

//Destructuring parameters from a call and preparing a message to write to a DB
const addMessage = ({received_message: message, userData: sender} = {}, next) => {

  //filling the schema
  let newMessage = new Message({
    psid: sender.id,
    messageSeq: message.seq,
    full_name: `${sender.first_name} ${sender.last_name}`,
    message: message.text,
    attachments: message.attachments,
    created: null
  });

  //saving the message
  newMessage.save((err) => {
    if (err) {
      return next(err);
    }
    // next(null)
  });
};

//A function to seatch for a message by author psid,
//may be refractured to be more polymorphic
const findMessage = (sender_psid, next) => {
  return new Promise((reject, resolve) => {
    Message.find({psid: sender_psid}, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    }).sort({_id: -1}).limit(1);
  })

};

module.exports = {
  addMessage,
  findMessage
}
